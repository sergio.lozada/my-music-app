import { AppState } from './../../../../src/app/state/app.state';
import { SpotifyService } from '../../../../src/app/feature/services/spotify.service';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ListTracksComponent } from '../../../../src/app/feature/pages/list-tracks/list-tracks.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { data } from './../../../data/list-tracks';

describe('ListTracksComponent', () => {
  let component: ListTracksComponent;
  let fixture: ComponentFixture<ListTracksComponent>;
  let service: SpotifyService;
  let store: MockStore<AppState>;
  const initialState = { tracks: [], favoritos: [] };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListTracksComponent],
      imports: [HttpClientTestingModule],
      providers: [SpotifyService, provideMockStore({ initialState })],
    }).compileComponents();

    fixture = TestBed.createComponent(ListTracksComponent);
    service = TestBed.inject(SpotifyService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('debe llamar al servicio de eliminar favoritos', () => {
    const track = data[0];
    track.clase = 'fa-solid';
    const spyOnEliminarFav = jest.spyOn(service, 'eliminarFavoritos');
    component.addFavoritos(track);
    expect(spyOnEliminarFav).toHaveBeenCalledWith(track.id);
  });

  it('debe llamar el servicio de guardar Favoritos', () => {
    const track = data[1];
    track.clase = undefined;
    fixture.detectChanges();
    const spyOnGuardarFav = jest.spyOn(service, 'guardarFavoritos');
    component.addFavoritos(track);
    expect(spyOnGuardarFav).toHaveBeenCalled();
  });
});
