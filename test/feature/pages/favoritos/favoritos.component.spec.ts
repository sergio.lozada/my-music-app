import { AppState } from './../../../../src/app/state/app.state';
import { SpotifyService } from '../../../../src/app/feature/services/spotify.service';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FavoritosComponent } from '../../../../src/app/feature/pages/favoritos/favoritos.component';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { data } from './../../../data/list-tracks';

describe('FavoritosComponent', () => {
  let component: FavoritosComponent;
  let fixture: ComponentFixture<FavoritosComponent>;
  let service: SpotifyService;
  let store: MockStore<AppState>;
  const initialState = { tracks: [], favoritos: [] };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FavoritosComponent],
      imports: [HttpClientTestingModule],
      providers: [SpotifyService, provideMockStore({ initialState })],
    }).compileComponents();

    fixture = TestBed.createComponent(FavoritosComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(SpotifyService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('debe llamar al servicio de eliminar favoritos', () => {
    const track = data[0];
    track.clase = 'fa-solid';
    const spyOnEliminarFav = jest.spyOn(service, 'eliminarFavoritos');
    component.addFavoritos(track);
    expect(spyOnEliminarFav).toHaveBeenCalledWith(track.id);
  });

  it('debe llamar el servicio de guardar Favoritos', () => {
    const track = data[1];
    track.clase = undefined;
    fixture.detectChanges();
    const spyOnGuardarFav = jest.spyOn(service, 'guardarFavoritos');
    component.addFavoritos(track);
    expect(spyOnGuardarFav).toHaveBeenCalled();
  });
});
