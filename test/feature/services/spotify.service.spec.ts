import { Track } from '../../../src/app/shared/interfaces/track.interface';
import { data } from '../../data/list-tracks';
import { TestBed } from '@angular/core/testing';
import { SpotifyService } from '../../../src/app/feature/services/spotify.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { Store } from '@ngrx/store';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { AppState } from 'src/app/state/app.state';
import { environment } from '../../../src/environments/environment';

describe('SpotifyService', () => {
  let service: SpotifyService;
  let httpMock: HttpTestingController;
  let store: MockStore<AppState>;
  const initialState = { tracks: [], favoritos: [] };

  const listTrack = data;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SpotifyService, provideMockStore({ initialState })],
    });
    service = TestBed.inject(SpotifyService);
    httpMock = TestBed.inject(HttpTestingController);
    store = TestBed.get<Store>(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('get list tracks and method get', () => {
    service.getListTracks().subscribe((resp) => {
      expect(resp).toEqual(listTrack);
    });

    const req = httpMock.expectOne(
      environment.API + `me/player/recently-played?limit=24`
    );

    expect(req.request.method).toBe('GET');
    req.flush(listTrack);
  });

  it('retorna un valor de tipo array', () => {
    const arrlistFAvoritos = service.getListFavoritos();
    expect(arrlistFAvoritos.length >= 0).toBe(true);
  });
});
