import { TestBed } from '@angular/core/testing';
import { AuthService } from '../../../src/app/auth/services/auth.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { environment } from '../../../src/environments/environment';

describe('AuthService', () => {
  let service: AuthService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthService],
    });
    service = TestBed.inject(AuthService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    httpMock.verify();
  });

  it('Service auth creado', () => {
    expect(service).toBeTruthy();
  });

  it('La url de la api es correcta', () => {
    const urlEndPoint = environment.authEndPoint;
    const resp = service.obtenerUrlLogin();
    expect(resp).toContain(urlEndPoint);
  });
});
