import { AuthService } from './../../../src/app/auth/services/auth.service';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginComponent } from '../../../src/app/auth/login/login.component';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let compiled: HTMLElement;
  let service: AuthService;
  let httpMock: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [HttpClientTestingModule],
      providers: [AuthService],
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(AuthService);
    fixture.detectChanges();
    compiled = fixture.nativeElement;
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('debe hacer match con el snapshot', () => {
    expect(compiled.innerHTML).toMatchSnapshot();
  });

  it('Si esxiste token debe llamar el servicio guardar token', () => {
    const spyObtenerTokenUrl = jest.spyOn(service, 'obtenerTokenUrl');
    component.verificarTokenUrl();
    expect(spyObtenerTokenUrl).toHaveBeenCalled();
  });
});
