export const environment = {
  API: 'https://api.spotify.com/v1/',
  production: true,
  clientId: '97ed3f68987e4880a49113c06bd0b9ad',
  authEndPoint: 'https://accounts.spotify.com/authorize',
  redirectUrl: 'https://my-music-app-nine.vercel.app/login/',
  scopes: [
    'user-read-currently-playing',
    'user-read-recently-played',
    'user-read-playback-state',
    'user-top-read',
    'user-modify-playback-state',
    'user-library-read',
    'playlist-read-private',
    'playlist-read-collaborative',
  ],
};
