import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganismsModule } from './components/organisms/organisms.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, OrganismsModule],
  exports: [OrganismsModule],
})
export class SharedModule {}
