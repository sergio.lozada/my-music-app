import { Artist } from './artist.interface';
import { Track } from './track.interface';

export interface ResponseSpotify {
  href: string;
  items: Item[];
  limit: number;
  next: null;
  offset: number;
  previous: null;
  total: number;
}

export interface Item {
  added_at: Date;
  track: Track;
}

export interface Album {
  album_type: string;
  artists: Artist[];
  available_markets: string[];
  external_urls: any;
  href: string;
  id: string;
  images: Image[];
  name: string;
  release_date: String;
  release_date_precision: string;
  total_tracks: number;
  type: string;
  uri: string;
}

export interface Image {
  height: number;
  url: string;
  width: number;
}
