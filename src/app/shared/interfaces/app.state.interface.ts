import { Track } from './track.interface';

export interface MusicState {
  tracks: ReadonlyArray<Track>;
  favoritos: ReadonlyArray<Track>;
}
