import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { reset } from '../../../../state/actions/tracks.actions';
import { AppState } from 'src/app/state/app.state';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  mostrarModal = false;

  constructor(private router: Router, private store: Store<AppState>) {}

  ngOnInit(): void {}

  logout() {
    localStorage.removeItem('user_token');
    this.store.dispatch(reset());
    this.router.navigate(['/login']);
  }
}
