import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Track } from 'src/app/shared/interfaces/track.interface';

@Component({
  selector: 'app-card-track',
  templateUrl: './card-track.component.html',
  styleUrls: ['./card-track.component.scss'],
})
export class CardTrackComponent implements OnInit {
  @Input() track!: Track;
  @Output() clickEvent = new EventEmitter<Track>();

  constructor() {}

  ngOnInit(): void {}

  onClick(track: Track) {
    this.clickEvent.emit(track);
  }
}
