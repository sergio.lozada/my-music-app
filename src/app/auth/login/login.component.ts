import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.verificarTokenUrl();
  }

  verificarTokenUrl() {
    const token = this.authService.obtenerTokenUrl();
    if (token) {
      this.authService.guardarToken(token);
      this.router.navigate(['/home']);
    }
  }

  ingresar() {
    const urlAuth = this.authService.obtenerUrlLogin();
    window.location.href = urlAuth;
  }
}
