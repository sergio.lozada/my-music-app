import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor() {}

  obtenerUrlLogin() {
    const authEndpoint = `${environment.authEndPoint}?`;
    const clientId = `client_id=${environment.clientId}&`;
    const redirectUrl = `redirect_uri=${environment.redirectUrl}&`;
    const scopes = `scope=${environment.scopes.join('%20')}&`;
    const responseType = `response_type=token&show_dialog=true`;
    return authEndpoint + clientId + redirectUrl + scopes + responseType;
  }

  obtenerTokenUrl() {
    if (!location.hash) {
      return '';
    }
    const params = location.hash.substring(1).split('&');
    return params[0].split('=')[1];
  }

  guardarToken(token: string) {
    localStorage.setItem('user_token', token);
  }
}
