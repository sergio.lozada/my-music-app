import { TestBed } from '@angular/core/testing';
import { AuthInterceptosService } from './auth-interceptos.service';

describe('AuthInterceptosService', () => {
  let service: AuthInterceptosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthInterceptosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
