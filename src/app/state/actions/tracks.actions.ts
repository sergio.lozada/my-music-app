import { createAction, props } from '@ngrx/store';
import { Track } from 'src/app/shared/interfaces/track.interface';

export const loadTracks = createAction('[Track List] Load Tracks');

export const loadedTracks = createAction(
  '[Track List] Loaded success',
  props<{ tracks: Track[] }>()
);

export const addFavoritos = createAction(
  '[add Favoritos], add favoritos',
  props<{ favoritos: Track[] }>()
);

export const updateFavoritos = createAction(
  '[update Favoritos], update favoritos',
  props<{ id: string; clase: string }>()
);

export const reset = createAction('[reset], reset Music App');
