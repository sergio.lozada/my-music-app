import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { SpotifyService } from 'src/app/feature/services/spotify.service';

@Injectable()
export class TracksEffects {
  loadTracks$ = createEffect(() =>
    this.actions$.pipe(
      ofType('[Track List] Load Tracks'),
      mergeMap(() =>
        this.spotifyServise.getListTracks().pipe(
          map((tracks) => ({
            type: '[Track List] Loaded success',
            tracks,
          })),
          catchError(() => EMPTY)
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private spotifyServise: SpotifyService
  ) {}
}
