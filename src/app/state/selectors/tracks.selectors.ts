import { createSelector } from '@ngrx/store';
import { MusicState } from 'src/app/shared/interfaces/app.state.interface';
import { AppState } from '../app.state';

export const selectTraks = (state: AppState) => state.stateMusicApp;

export const selectListTraks = createSelector(
  selectTraks,
  (state: MusicState) => state.tracks
);

export const selectListFavoritos = createSelector(
  selectTraks,
  (state: MusicState) => state.favoritos
);
