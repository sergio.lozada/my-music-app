import { ActionReducerMap } from '@ngrx/store';
import { MusicState } from '../shared/interfaces/app.state.interface';
import { tracksReducer } from './reducers/tracks.reducers';

export interface AppState {
  stateMusicApp: MusicState;
}

export const ROOT_REDUCERS: ActionReducerMap<AppState> = {
  stateMusicApp: tracksReducer,
};
