import { createReducer, on } from '@ngrx/store';
import { MusicState } from 'src/app/shared/interfaces/app.state.interface';
import {
  addFavoritos,
  loadedTracks,
  loadTracks,
  reset,
  updateFavoritos,
} from '../actions/tracks.actions';

export const initialState: MusicState = {
  tracks: [],
  favoritos: [],
};

export const tracksReducer = createReducer(
  initialState,
  on(loadTracks, (state) => {
    return { ...state };
  }),

  on(loadedTracks, (state, { tracks }) => {
    return { ...state, tracks };
  }),

  on(addFavoritos, (state, { favoritos }) => {
    return { ...state, favoritos };
  }),

  on(reset, (state) => initialState),

  on(updateFavoritos, (state, { id, clase }) => {
    const listTracks = state.tracks.map((track) => {
      if (track.id === id) {
        return {
          ...track,
          clase,
        };
      } else {
        return track;
      }
    });
    return { ...state, tracks: listTracks };
  })
);
