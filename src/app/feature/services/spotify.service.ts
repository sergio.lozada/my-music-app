import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { map } from 'rxjs';
import {
  Item,
  ResponseSpotify,
} from 'src/app/shared/interfaces/spotify.interface';
import { Track } from 'src/app/shared/interfaces/track.interface';
import { addFavoritos } from '../../state/actions/tracks.actions';
import { AppState } from 'src/app/state/app.state';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SpotifyService {
  private API_URL = environment.API;

  constructor(private http: HttpClient, private store: Store<AppState>) {}

  getListTracks() {
    let params = new HttpParams();
    params = params.set('limit', '24');
    return this.http
      .get<ResponseSpotify>(this.API_URL + 'me/player/recently-played', {
        params,
      })
      .pipe(
        map(({ items }) =>
          items.map(({ track }) => {
            const arrFavoritos = this.getListFavoritos();
            for (let i = 0; i < arrFavoritos.length; i++) {
              if (track.id === arrFavoritos[i].id) {
                track.clase = 'fa-solid';
              }
            }
            return track;
          })
        )
      );
  }

  getListFavoritos() {
    let arrFavoritos: any = localStorage.getItem('favoritos');
    if (arrFavoritos) {
      arrFavoritos = JSON.parse(arrFavoritos);
    } else {
      arrFavoritos = [];
    }
    return arrFavoritos;
  }

  guardarFavoritos(track: Track) {
    let arrFavoritos = this.getListFavoritos();
    arrFavoritos.push(track);
    localStorage.setItem('favoritos', JSON.stringify(arrFavoritos));
    this.store.dispatch(addFavoritos({ favoritos: arrFavoritos }));
  }

  eliminarFavoritos(id: string) {
    let arrFavoritos = this.getListFavoritos();
    arrFavoritos = arrFavoritos.filter((track: Track) => track.id !== id);
    localStorage.setItem('favoritos', JSON.stringify(arrFavoritos));
    this.store.dispatch(addFavoritos({ favoritos: arrFavoritos }));
  }
}
