import { Component, OnInit } from '@angular/core';
import { Track } from 'src/app/shared/interfaces/track.interface';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-favoritos',
  templateUrl: './favoritos.component.html',
  styleUrls: ['./favoritos.component.scss'],
})
export class FavoritosComponent implements OnInit {
  listTracks = [];

  constructor(private spotifyService: SpotifyService) {}

  ngOnInit(): void {
    this.cargarListaFavoritos();
  }

  cargarListaFavoritos() {
    this.listTracks = this.spotifyService.getListFavoritos();
  }

  addFavoritos(track: Track) {
    if (track.clase === 'fa-solid') {
      track.clase = 'fa-regular';
      this.spotifyService.eliminarFavoritos(track.id);
      this.cargarListaFavoritos();
    } else {
      track.clase = 'fa-solid';
      this.spotifyService.guardarFavoritos(track);
    }
  }
}
