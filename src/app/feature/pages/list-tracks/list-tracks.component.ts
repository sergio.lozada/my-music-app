import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Track } from 'src/app/shared/interfaces/track.interface';
import { AppState } from 'src/app/state/app.state';
import { SpotifyService } from '../../services/spotify.service';
import { selectListTraks } from './../../../state/selectors/tracks.selectors';
import {
  loadTracks,
  updateFavoritos,
} from './../../../state/actions/tracks.actions';

@Component({
  selector: 'app-list-tracks',
  templateUrl: './list-tracks.component.html',
  styleUrls: ['./list-tracks.component.scss'],
})
export class ListTracksComponent implements OnInit {
  listTracks$: Observable<any> = new Observable();

  constructor(
    private spotifyService: SpotifyService,
    private store: Store<AppState>
  ) {}

  ngOnInit(): void {
    this.cargarListaDeCanciones();
  }

  cargarListaDeCanciones() {
    this.store.dispatch(loadTracks());
    this.listTracks$ = this.store.select(selectListTraks);
  }

  addFavoritos(track: Track) {
    const copyTrack = { ...track };
    if (copyTrack.clase === 'fa-solid') {
      copyTrack.clase = 'fa-regular';
      this.spotifyService.eliminarFavoritos(copyTrack.id);
      this.store.dispatch(
        updateFavoritos({ id: copyTrack.id, clase: copyTrack.clase })
      );
    } else {
      copyTrack.clase = 'fa-solid';
      this.spotifyService.guardarFavoritos(copyTrack);
      this.store.dispatch(
        updateFavoritos({ id: copyTrack.id, clase: copyTrack.clase })
      );
    }
  }
}
