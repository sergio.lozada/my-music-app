import { AppState } from './../../../state/app.state';
import { HeaderComponent } from './../../../shared/components/organisms/header/header.component';
import { LoadingComponent } from './../../../shared/components/organisms/loading/loading.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let store: MockStore<AppState>;
  const initialState = { tracks: [], favoritos: [] };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomeComponent, LoadingComponent, HeaderComponent],
      providers: [provideMockStore({ initialState })],
      imports: [],
    }).compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
