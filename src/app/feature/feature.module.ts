import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeatureRoutingModule } from './feature-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { FavoritosComponent } from './pages/favoritos/favoritos.component';
import { SharedModule } from '../shared/shared.module';
import { ListTracksComponent } from './pages/list-tracks/list-tracks.component';

@NgModule({
  declarations: [HomeComponent, FavoritosComponent, ListTracksComponent],
  imports: [CommonModule, FeatureRoutingModule, SharedModule],
  exports: [FeatureRoutingModule],
})
export class FeatureModule {}
